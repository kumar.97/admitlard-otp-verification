import React from 'react'
import {Switch, Route} from "react-router";
import NotFound from "./NotFound";
import SEO from "../Common/Component/Helmet";
import Screens from "./Screens";
import './all.sass'

const Home = () => {
  return(
    <React.Fragment>

      <SEO />

      <Switch>
        <Route
          exact
          path={'/'}
          render={props => (
            <Screens {...props} />
          )}
        />

        <Route
          exact
          render={()=>(
            <NotFound />
          )}
        />
      </Switch>

    </React.Fragment>
  )
}

export default Home