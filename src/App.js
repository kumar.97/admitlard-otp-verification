import React from 'react';
import Home from "./Home/Home";
import {withRouter} from "react-router-dom";

function App() {
  const WrapperHome = withRouter(props => (
    <Home
      {...props}
    />
  ))
  return <WrapperHome />
}

export default App;
