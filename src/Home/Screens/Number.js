import React, {useState} from "react";
import {Typography, Button, Form, InputNumber, Input, Col, Layout, Row, Divider} from "antd";
import image from '../../Common/Images/logo-full.png'
import SendNotification from "../../Common/Utils/SendNotification";
import NotificationTypeEnum from "../../Common/Model/NotificationTypeEnum";
import PhoneInput from "react-phone-input-2";

const {Footer} = Layout
const {Title} = Typography

const formSpan = {
  xs: {
    span: 20
  }
}

const Number = ({setPhone, setKey}) => {
  const [number, setNumber] = useState(null)
  const [country] = useState('us')

  function Next() {
    if( !number || number.toString().length!==12 ) {
      SendNotification(NotificationTypeEnum.Failure,  "Input Valid Phone Number")
    } else {
      localStorage.setItem('otp', '9876')
      SendNotification(NotificationTypeEnum.Success, "OTP has been sent to " + number)
      setPhone(number)
      setKey(2)
    }
  }

  function changeNumber(x) {
    setNumber(x)
  }

  return(
    <Layout className="is-fullheight">
      <Row justify="center">
        <Col xs={{span: 20}} md={{span: 14}} lg={{span: 8}} >
          <Row type="flex" justify="center" >
            <Col xs={{span: 20}} className="is-quarter-top has-text-centered">
              <img
                width="60%"
                className="padded-top"
                src={image}
                alt="AdmitKard"
              />
            </Col>

            <Col className="head has-text-centered">
              <Title className="is-normal" >
                Welcome Back
              </Title>
              <Title
                level={4}
                className="is-normal is-gray"
              >
                Please sign in to your account
              </Title>
            </Col>

            <Col xs={{span: 20}} lg={{span: 16}} md={{span: 18}} >
              <Form {...formSpan} >
                <Form.Item>
                  <Divider orientation="left" >Enter Contact Number</Divider>
                  <PhoneInput
                    value={country}
                    country={'in'}
                    preserveOrder={['onlyCountries', 'preferredCountries']}
                    onChange={e=>changeNumber(e)}
                    onEnterKeyPress={Next}
                    enableSearch
                  />
                </Form.Item>
              </Form>
              <p className="has-text-centered">
                We will send you a one time SMS message. Charges may apply.
              </p>
            </Col>

            <Footer style={{background: "inherit"}} >
              <Button onClick={Next} shape="round" type="primary gold" size="large" >
                Sign In with OTP
              </Button>
            </Footer>
          </Row>
        </Col>
      </Row>

    </Layout>
  )
}

export default Number