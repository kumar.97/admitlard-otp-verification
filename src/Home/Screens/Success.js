import React from "react";
import {Button, Col, Form, InputNumber, Layout, Row, Typography} from "antd";
import image from '../../Common/Images/success.png'

const {Content, Header, Footer} = Layout
const {Title} = Typography

const Success = () => {
  return(
    <Layout className="is-fullheight">
      <Row justify="center">
        <Col xs={{span: 20}} md={{span: 14}} lg={{span: 8}} >
          <Row type="flex" className="has-text-centered" justify="center" >
            <Col className="is-quarter-top">
              <img
                width="80%"
                style={{
                  paddingBottom: '10%'
                }}
                className="padded-top-z"
                src={image}
                alt="AdmitKard"
              />
            </Col>
            <Col className="head">
              <Title level={2}>
                Welcome to AdmitKard
              </Title>
              <Title level={4} className="is-normal is-gray" >
                In order to provide you with a custom experience,
                <span style={{color: '#999'}}>we need to ask you a few questions.</span>
              </Title>
            </Col>

            <Footer style={{background: "inherit"}} >
              <Button shape="round" type="primary gold" size="large" >
                Get Started
              </Button>
              <p>*This will only take 5 minutes</p>
            </Footer>
          </Row>
        </Col>
      </Row>

    </Layout>
  )
}

export default Success