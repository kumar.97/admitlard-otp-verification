import React, {useState} from "react"
import Number from "./Number";
import Otp from "./Otp";
import Success from "./Success";

const Screens = () => {
  const [key, setKey] = useState(1)
  const [phone, setPhone] = useState(null)

  function getComponent() {
    switch (key) {
      case 1: return <Number setKey={setKey} setPhone={setPhone} />
      case 2: return <Otp setKey={setKey} phone={phone} setPhone={setPhone} />
      case 3: return <Success />
    }
  }

  return(
    <div>
      {getComponent()}
    </div>
  )
}

export default Screens
