import React, {useEffect, useState} from "react";
import {Button, Col, Form, Layout, Row, Typography} from "antd";
import image from '../../Common/Images/verify.png'
import OtpInput from 'react-otp-input'
import SendNotification from "../../Common/Utils/SendNotification";
import NotificationTypeEnum from "../../Common/Model/NotificationTypeEnum";
import Sleep from "../../Common/Utils/Sleep";

const {Title} = Typography
const {Content, Header, Footer} = Layout

const Otp = ({phone, setKey}) => {
  const [otp, setOtp] = useState(null)
  const [time] = useState(20000)
  const [valid, setValid] = useState(true)

  function Next() {
    if(!valid) {
      SendNotification(NotificationTypeEnum.Failure, "Generate New OTP and continue")
    } else {
      if(!otp) {
        SendNotification(NotificationTypeEnum.Failure, "Enter Valid OTP to Login")
      } else {
        if(otp.toString().length!==4) {
          SendNotification(NotificationTypeEnum.Failure, "Enter Valid OTP to Login")
        } else if(otp===localStorage.getItem('otp')) {
          setValid(false)
          SendNotification(NotificationTypeEnum.Success, "Login Successful")
          setKey(3)
        } else {
          SendNotification(NotificationTypeEnum.Failure, "Wrong OTP")
        }
      }
    }
  }

  function removeOtp() {
    localStorage.removeItem('otp')
    SendNotification(NotificationTypeEnum.Failure, 'OTP Expired')
    setValid(false)
  }

  useEffect(()=> {
    setValid(true)
    localStorage.setItem('g', 'good')
    localStorage.setItem('ex', `${(Date.now()+time)/1000}`)
    OTPInvalid()

    return () => {
      localStorage.removeItem('g')
      localStorage.removeItem('ex')
    }
  }, [])

  function OTPInvalid() {
    SendNotification(NotificationTypeEnum.Success, 'OTP will expire in '+(time/1000)+' seconds')
    Sleep(time)
      .then(()=> {
        console.log(parseInt(localStorage.getItem('ex')), Date.now()/1000)
        if (
          localStorage.getItem('g')==='good' &&
          parseInt(localStorage.getItem('ex')) <= Math.floor(Date.now()/1000)
        ) {
          removeOtp()
        }
      })
  }

  function genOtp() {
    setValid(true)
    console.log((Date.now()+time)/1000)
    SendNotification(NotificationTypeEnum.Success, "New OTP send to +"+phone)
    localStorage.setItem('otp', '9876')
    localStorage.setItem('ex', `${((Date.now()+time)/1000)}`)
    OTPInvalid()
  }

  return(
    <Layout className="is-fullheight">
      <Row justify="center">
        <Col xs={{span: 20}} md={{span: 14}} lg={{span: 8}} >
          <Row type="flex" justify="center" >
            <Col xs={{span: 20}} className="is-quarter-top has-text-centered">
              <img
                width="60%"
                className="padded-top padded-top-z"
                src={image}
                alt="AdmitKard"
              />
            </Col>

            <Col className="head has-text-centered">
              <Title level={3} className="is-normal is-gray" >
                Please verify Mobile number
              </Title>
              <Title level={4} className="is-gray is-normal" >
                <span>An OTP is sent to </span>{'+'+phone}
                <br />
                <Button onClick={()=>setKey(1)} type="text" style={{color: '#F7B348', stroke: '#F7B348'}} >
                  Change phone number
                </Button>
                <br />
                  <Button onClick={genOtp} type="text" style={{color: '#F7B348', stroke: '#F7B348'}}>
                    {valid ? "Didn't Received OTP?" : "Send New OTP"}
                  </Button>
              </Title>
            </Col>

            <Content className="has-text-centered" >
              {valid ? (
                <Form>
                <Form.Item>
                  <OtpInput
                    containerStyle={{
                      justifyContent: 'center'
                    }}
                    inputStyle={{
                      padding: '10px 5px',
                      width: '2.5em'
                    }}
                    shouldAutoFocus
                    isInputNum
                    value={otp}
                    onChange={e=>setOtp(e)}
                    numInputs={4}
                    separator={<span>&nbsp;&nbsp;&nbsp;</span>}
                  />
                </Form.Item>
              </Form>) : null}
              <p className="has-text-centered">
                We will send you a one time SMS message. Charges may apply.
              </p>
            </Content>

            <Footer style={{background: "inherit"}} >
              <Button onClick={Next} shape="round" type="primary gold" size="large" >
                Sign In with OTP
              </Button>
            </Footer>
          </Row>
        </Col>
      </Row>

    </Layout>
  )
}

export default Otp