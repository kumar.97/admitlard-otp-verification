## OTP Verification For AdmitKard

- Project is deployed on [Netlify](https://otp-vertification.netlify.app/)

#### Author [@aman](https://gitlab.com/kumar.97)

- Project uses ReactJS for UI-Interface
- Otp is matched via localstorage
- Handles scenarios of wrong OTPs and validates mobile number

### Steps to See the Project

1. Go to [project link](https://otp-vertification.netlify.app/)
2. Enter a Contact Number, invalid inputs (non-numerics) won't be accepted.
3. You can choose country from the dropdown menu
4. The App currently validates a number only if it is 12 chars long( including country code). So, preferably just type a 10 digit random number and let India in the default country as it is.
5. Now, the OTP Verification screen will appear. The OTP will be expired after 20 seconds and the otp Input would disappear. 
6. If the OTP Expires, Click on Send New OTP Button It will generate a New Otp and the otp input would appear again.
7. If the Otp isn't recieved Click on Didn't Received Otp and a new OTP will be generated and the expiry time will also be refreshed.
7. THe Change phone number button, will bring back you to the mobile number input screen
8. Any non valid Input would raise error, only 9876 will be accepted(stored in localstorage). Input 9876 and click on Sign In with OTP button.
9. On successfully entering OTP, you will be brought to the success page

## Available Scripts

In the project directory, you can run:

### `yarn start`

### `yarn build`
