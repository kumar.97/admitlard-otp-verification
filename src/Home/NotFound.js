import React from "react";
import {Typography} from "antd";

const {Title} = Typography
const NotFound = () => {
  return(
    <Title>
      Page Not Exists
    </Title>
  )
}

export default NotFound