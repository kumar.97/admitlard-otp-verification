import React from "react";
import {Helmet} from "react-helmet";

const title = 'OTP Handler'
const description = 'React App to Handle OTP Verification'

function SEO() {
  return(
    <Helmet>
      <meta lang={'en'} />
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="description" content={description} />
      <meta name="viewport" content="'width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no'" />

    </Helmet>
  )
}

export default SEO